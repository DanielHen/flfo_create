import os
import datetime

"""
This is a simple script to create a dir structure with empty files. It has been created in
order to simplify of  demonstration how  SSIS_Dispatcher works(git path). It creates two folders
FROM and TO. FROM folder consists of files, files names are based on lst list. TO folder consists of
subfolders based on lst list.
folder
"""

fromDir = "C:\SSIS_Dispatcher\FROM\\" #FROM directory where files are stored
toDir =  "C:\SSIS_Dispatcher\TO\\" #TO, in subfolders of TO all files will be dispatched by SSIS_Dipatcher.
lst = ['WKRM', 'LARK', 'MAT', 'HOK'] #Based on this list, subfolders within toDir will be crated.

#This function creates files based on lst list.
def createFile(dir,num_date,level = None):
    fileName = dir.split('\\')[-1]  # Create a file name
    if level == 0:
        print(dir)
        print(fileName)
        print(dir + "ab " + fileName)
        open(os.path.join(dir, '{}{}.txt'.format(fileName,num_date)), "a").close()
    else:
        dir = dir.rsplit('\\',1)[0]
        print(fileName)
        open(os.path.join(dir, '{}{}.txt'.format(fileName, num_date)), "a").close()


#This function loop through dates between d1 and d2.
def dateRange(d1, d2):
    return (d1 + datetime.timedelta(days=i) for i in range((d2 - d1).days + 1))


def mainFun(lst,dir,file = None,structure = None):
    copy_lst = lst.copy()
    # print(copy_lst)
    copy_lst = ['{}{}'.format(dir,i) for  i in copy_lst]
    # print(copy_lst)
    main_dir = list(dir)
    start_date = datetime.datetime(2019, 12,1) #put a start day if you want create files
    end_date = datetime.datetime(2019,12,12)   #put a end dat if you want to create files
    # print(dir)
    if structure == 1: # Run if additinal parameter structure is 1.
        for dir1 in main_dir:
                for dir2 in copy_lst:
                    try:
                        os.makedirs(os.path.join(dir1,dir2))
                        if file == 1:  # Run if additinal parameter file is 1.
                            for d in dateRange(start_date, end_date):
                                createFile(dir2,d.strftime('%Y%m%d'),1) #Initiazing createFile function

                    except OSError: pass
    else:
        print(dir)
        try:
            os.makedirs(dir)
            for d in dateRange(start_date, end_date):
                for dir2 in copy_lst:
                    createFile(dir2, d.strftime('%Y%m%d'))
        except  OSError: pass

"""
Initiazing Script. 
"""
mainFun(lst,fromDir) #Creates FROM folder with files
mainFun(lst,toDir,0,1) #Creates TO with subfolders


